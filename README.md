# README #


### What is this repository for? ###

* The files here provide support for unit testing code that calls `assert`, `precondition`, `assertionFailure`, `preconditionFailure`, or `fatalError`.

### How do I get set up? ###

* Add *AssertTest.swift* to any module that you wish to test. The local versions of the `assert` functions in *AssertTest.swift* will be linked to your code instead of the swift versions.
* Add *XCTAssert+AssertTest.swift* to your test target and use the provided `XCTAssert*` calls to test your asserts.
* If desired, add *AssertTestUnitTests.swift* to a test target to test the AssertTest code.

