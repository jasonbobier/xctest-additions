//
//  Copyright © 2021 Jason Bobier. All rights reserved.
//

import XCTest

#if DEBUG

@testable import CoquereKit

class AssertTestUnitTests: XCTestCase {
	func test_assert__Bool__String_file_line() {
		XCTContext.runActivity(named: "calls assert with the correct message if condition is false") { activity in
			XCTAssertFailsAssert(CoquereKit.assert(false))
			XCTAssertFailsAssert(CoquereKit.assert(false, "test message"), "test message")
		}

		XCTContext.runActivity(named: "doesn't call assert if condition is true") { activity in
			XCTAssertPassesAssert(CoquereKit.assert(true))
		}
	}

	func test_precondition__Bool__String_file_line() {
		XCTContext.runActivity(named: "calls precondition with the correct message if condition is false") { activity in
			XCTAssertFailsPrecondition(CoquereKit.precondition(false))
			XCTAssertFailsPrecondition(CoquereKit.precondition(false, "test message"), "test message")
		}

		XCTContext.runActivity(named: "doesn't call precondition if condition is true") { activity in
			XCTAssertPassesPrecondition(CoquereKit.precondition(true))
		}
	}

	func test_assertionFailure__String_file_line() {
		XCTContext.runActivity(named: "calls assertionFailure with the correct message") { activity in
			XCTAssertAssertionFailure(CoquereKit.assertionFailure())
			XCTAssertAssertionFailure(CoquereKit.assertionFailure("test message"), "test message")
		}
	}

	func test_preconditionFailure__String_file_line() {
		XCTContext.runActivity(named: "calls preconditionFailure with the correct message") { activity in
			XCTAssertPreconditionFailure(CoquereKit.preconditionFailure())
			XCTAssertPreconditionFailure(CoquereKit.preconditionFailure("test message"), "test message")
		}
	}

	func test_fatalError__String_file_line() {
		XCTContext.runActivity(named: "calls fatalError with the correct message") { activity in
			XCTAssertFatalError(CoquereKit.fatalError())
			XCTAssertFatalError(CoquereKit.fatalError("test message"), "test message")
		}
	}
}

#endif
