//
//  Copyright © 2021 Jason Bobier. All rights reserved.
//

import Foundation

#if DEBUG

private enum AssertTest: String {
	case assert
	case precondition
	case assertionFailure
	case preconditionFailure
	case fatalError

	private static let threadStorageKey = "AssertTestThreadStorageKey"

	static var completion: ((String, String) -> Never)? {
		get {
			Thread.current.threadDictionary[Self.threadStorageKey] as? (String, String) -> Never
		}

		set {
			Thread.current.threadDictionary[Self.threadStorageKey] = newValue
		}
	}
}

func assert(_ condition: @autoclosure () -> Bool, _ message: @autoclosure () -> String = String(), file: StaticString = #file, line: UInt = #line) {
	if let completion = AssertTest.completion {
		if !condition() {
			completion(AssertTest.assert.rawValue, message())
		}
	} else {
		Swift.assert(condition(), message(), file: file, line: line)
	}
}

func precondition(_ condition: @autoclosure () -> Bool, _ message: @autoclosure () -> String = String(), file: StaticString = #file, line: UInt = #line) {
	if let completion = AssertTest.completion {
		if !condition() {
			completion(AssertTest.precondition.rawValue, message())
		}
	} else {
		Swift.precondition(condition(), message(), file: file, line: line)
	}
}

func assertionFailure(_ message: @autoclosure () -> String = String(), file: StaticString = #file, line: UInt = #line) {
	if let completion = AssertTest.completion {
		completion(AssertTest.assertionFailure.rawValue, message())
	} else {
		Swift.assertionFailure(message(), file: file, line: line)
	}
}

func preconditionFailure(_ message: @autoclosure () -> String = String(), file: StaticString = #file, line: UInt = #line) -> Never {
	if let completion = AssertTest.completion {
		completion(AssertTest.preconditionFailure.rawValue, message())
	} else {
		Swift.preconditionFailure(message(), file: file, line: line)
	}
}

func fatalError(_ message: @autoclosure () -> String = String(), file: StaticString = #file, line: UInt = #line) -> Never {
	if let completion = AssertTest.completion {
		completion(AssertTest.fatalError.rawValue, message())
	} else {
		Swift.fatalError(message(), file: file, line: line)
	}
}

#endif
