//
//  Copyright © 2021 Jason Bobier. All rights reserved.
//

import Foundation
import XCTest

private enum AssertTest: String {
	case assert
	case precondition
	case assertionFailure
	case preconditionFailure
	case fatalError

	private static let threadStorageKey = "AssertTestThreadStorageKey"

	static var completion: ((String, String) -> Never)? {
		get {
			Thread.current.threadDictionary[Self.threadStorageKey] as? (String, String) -> Never
		}

		set {
			Thread.current.threadDictionary[Self.threadStorageKey] = newValue
		}
	}
}

private class WaiterDelegate: NSObject, XCTWaiterDelegate {
	let assertTest: AssertTest
	let file: StaticString
	let line: UInt
	let timeout: Double

	static let defaultTimeout = 2.0

	init(assertTest: AssertTest, file: StaticString, line: UInt, timeout: Double = defaultTimeout) {
		self.assertTest = assertTest
		self.file = file
		self.line = line
		self.timeout = timeout
		super.init()
	}

	func waiter(_ waiter: XCTWaiter, didTimeoutWithUnfulfilledExpectations unfulfilledExpectations: [XCTestExpectation]) {
		XCTFail("Exceeded timeout of \(timeout) seconds while waiting for \(assertTest.rawValue)", file: file, line: line)
	}
	
	func waiter(_ waiter: XCTWaiter, fulfillmentDidViolateOrderingConstraintsFor expectation: XCTestExpectation, requiredExpectation: XCTestExpectation) {
		XCTFail("Unexpected error")
	}

	func waiter(_ waiter: XCTWaiter, didFulfillInvertedExpectation expectation: XCTestExpectation) {
		XCTFail("Unexpected error")
	}

	func nestedWaiter(_ waiter: XCTWaiter, wasInterruptedByTimedOutWaiter outerWaiter: XCTWaiter) {
		XCTFail("Unexpected error")
	}
}

public func XCTAssertPassesAssert<T>(_ expression: @escaping @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", timeout: Double? = nil, file: StaticString = #filePath, line: UInt = #line) {
	testAssertFailure(expression, assertTest: AssertTest.assert, isInverted: true, assertionMessage: message(), timeout: timeout, file: file, line: line)
}

public func XCTAssertFailsAssert<T>(_ expression: @escaping @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", timeout: Double? = nil, file: StaticString = #filePath, line: UInt = #line) {
	testAssertFailure(expression, assertTest: AssertTest.assert, isInverted: false, assertionMessage: message(), timeout: timeout, file: file, line: line)
}

public func XCTAssertPassesPrecondition<T>(_ expression: @escaping @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", timeout: Double? = nil, file: StaticString = #filePath, line: UInt = #line) {
	testAssertFailure(expression, assertTest: AssertTest.precondition, isInverted: true, assertionMessage: message(), timeout: timeout, file: file, line: line)
}

public func XCTAssertFailsPrecondition<T>(_ expression: @escaping @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", timeout: Double? = nil, file: StaticString = #filePath, line: UInt = #line) {
	testAssertFailure(expression, assertTest: AssertTest.precondition, isInverted: false, assertionMessage: message(), timeout: timeout, file: file, line: line)
}

public func XCTAssertAssertionFailure<T>(_ expression: @escaping @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", timeout: Double? = nil, file: StaticString = #filePath, line: UInt = #line) {
	testAssertFailure(expression, assertTest: AssertTest.assertionFailure, isInverted: false, assertionMessage: message(), timeout: timeout, file: file, line: line)
}

public func XCTAssertPreconditionFailure<T>(_ expression: @escaping @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", timeout: Double? = nil, file: StaticString = #filePath, line: UInt = #line) {
	testAssertFailure(expression, assertTest: AssertTest.preconditionFailure, isInverted: false, assertionMessage: message(), timeout: timeout, file: file, line: line)
}

public func XCTAssertFatalError<T>(_ expression: @escaping @autoclosure () throws -> T, _ message: @autoclosure () -> String = "", timeout: Double? = nil, file: StaticString = #filePath, line: UInt = #line) {
	testAssertFailure(expression, assertTest: AssertTest.fatalError, isInverted: false, assertionMessage: message(), timeout: timeout, file: file, line: line)
}

private func testAssertFailure<T>(_ expression: @escaping () throws -> T, assertTest: AssertTest, isInverted: Bool, assertionMessage: String, timeout: Double?, file: StaticString, line: UInt) {
	let expectation = XCTestExpectation(description: "\(assertTest.rawValue) assertion: \(assertionMessage)\(!assertionMessage.isEmpty ? " " : "")file: \(file) line: \(line)")
	let waiterDelegate = WaiterDelegate(assertTest: assertTest, file: file, line: line, timeout: timeout ?? WaiterDelegate.defaultTimeout)

	expectation.isInverted = isInverted
	Thread {
		AssertTest.completion = { (type: String, message: String) -> Never in
			XCTAssertEqual(type, assertTest.rawValue, file: file, line: line)
			XCTAssertEqual(message, assertionMessage, file: file, line: line)
			expectation.fulfill()
			Thread.exit()
			fatalError()
		}

		_ = try? expression()
	}.start()
	XCTWaiter(delegate: waiterDelegate).wait(for: [expectation], timeout: waiterDelegate.timeout)
}
